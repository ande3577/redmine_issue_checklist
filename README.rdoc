= issue_checklist

A fork of the Remdine Issue Checklist Plugin by Kirill Bezrukov.  www.redmine.org/plugins/issue_checklist

Tested with the 1.4 stable branch of redmine.

== Test
rake test:plugins PLUGIN=redmine_issue_checklist RAILS_ENV=test_sqlite3